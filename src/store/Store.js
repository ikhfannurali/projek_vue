import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://dashboard-keluargasehat.kemkes.go.id/rest/'

export const store = new Vuex.Store({
    state:{
        token : localStorage.getItem('access_token'),
        user:'',
        isi:[]
    },
    getters:{
        loggedIn(state){
            return state.token
        },
        loggedUser(state){
            return state.user
        },
        isi(state){
            return state.isi
        }
    },
    mutations:{
        retrievetoken(state, token){
            state.token = token
        },
        retrieveuser(state, user){
            state.user = user
        },
        retrievedata(state, isi){
            state.isi = isi
        }
    },
    actions:{
        retrieveToken(context, credentials){
            axios.post('api/auth/login',{
                username : credentials.username,
                password : credentials.password,
            })
            .then(response => {
                const token = response.data.data.token
                const tes = response.config.data
                console.log(token)
                localStorage.setItem('access_token', token)
                context.commit('retrievetoken', token)
                // console.log(localStorage.getItem('access_token'))

            })
            .catch(error => {
                console.log("error")
            })
        },
        retrieveUser(context){
            var config = {
                headers: {  
                            Authorization: 'Bearer '+localStorage.getItem('access_token'),
                            'Content-Type': 'application/json',    
                         }
            };
            axios.get('api/auth/user',config)
            .then(response => {
                const user = response.data.data.name
                context.commit('retrieveuser', user)
            })
            .catch(error => {
                console.log(error)
            })
        },
        retrieveData(context){
            var config = {
                headers: {  'access-control-allow-origin': '*',
                            Authorization : 'Bearer '+localStorage.getItem('access_token'),
                            'content-type': 'application-json',    
                         },
            };
            axios.get('api/report?limit=5&offset=0', config )
            .then(response => {
                const isi = response.data.data
                context.commit('retrievedata', isi)
            })
            .catch(error => {
                console.log(error)
            })
        },
        retrieveProvinsi(context, prov){
            var config = {
                headers: {  'access-control-allow-origin': '*',
                            Authorization : 'Bearer '+localStorage.getItem('access_token'),
                            'content-type': 'application-json',    
                         },
            };
            // console.log(prov)
            axios.get('api/report?provinsi='+prov+'&limit=5&offset=0', config)
            .then(response => {
                console.log(response.data.data)
                const isi = response.data.data
                context.commit('retrievedata', isi)
            })
            .catch(error => {
                console.log(error)
            })
        },
        retrieveKota(context, kota){
            var config = {
                headers: {  'access-control-allow-origin': '*',
                            Authorization : 'Bearer '+localStorage.getItem('access_token'),
                            'content-type': 'application-json',    
                         },
            };
            console.log(kota)
            axios.get('api/report?provinsi='+kota.provinsi+'&kota='+kota.kota+'&limit=5&offset=0', config)
            .then(response => {
                console.log(response.data.data)
                const isi = response.data.data
                context.commit('retrievedata', isi)
            })
            .catch(error => {
                console.log(error)
            })
        },
        destroyToken(){
            var config = {
                headers: {  
                            Authorization: 'Bearer '+localStorage.getItem('access_token'),
                            'Content-Type': 'application/json',    
                         }
            };
            axios.delete('api/auth/invalidate',config)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
        },
    }
})