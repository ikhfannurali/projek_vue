import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/login'
import Register from '@/components/register'
import Admin from '@/components/admin'
import Admin2 from '@/components/admin2'
import Dashboard from '@/components/content/dashboard'
import Tabel from '@/components/content/tabel'

Vue.use(Router)

const router = new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    }, 
    {
      path: '/admin2',
      name: 'admin2',
      component: Admin2,
      children:[
        {
          path: "dashboard",
          name: "dashboard",
          component: Dashboard
        },
        {
          path: "tabel",
          name: "tabel",
          component: Tabel
        }
      ]
    }, 
  ]
})
export default router
